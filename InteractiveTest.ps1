
#Load functions

$LibraryRoot="$(Split-Path $myinvocation.Mycommand.path -Parent)"
Write-Host "Loading $LibraryRoot\DataTranformationLibrary.ps1"
. "$LibraryRoot\DataTranformationLibrary.ps1"


Write-host "`n`n--------------------Test 1--------------------"
$TestString="(id,created,employee(id,firstname,employeeType(id), lastname),location)"
Write-output "Original:`n  $TestString"
write-output "JSON String:`n  $(Convert-CustomToJSON $TestSTring)"
write-output "PS Object:`n  $(Convert-JSONToPSObject "$(Convert-CustomToJSON $TestString)")"
write-output "Property Name Output:"
Write-PropertyNames $(Convert-JSONToPSObject "$(Convert-CustomToJSON $TestString)")
Write-host "`n`n--------------------Test 1: Aggregate Function--------------------"
Format-CustomString $TestString


Write-host "`n`n--------------------Test 2--------------------"
$TestString="(id,created,employee(id,firstname,employeeType(id), lastname,middlename),location,GPSCoords)"
Write-output "Original:`n  $TestString"
write-output "JSON String:`n  $(Convert-CustomToJSON $TestSTring)"
write-output "PS Object:`n  $(Convert-JSONToPSObject "$(Convert-CustomToJSON $TestString)")"
write-output "Property Name Output:"
Write-PropertyNames $(Convert-JSONToPSObject "$(Convert-CustomToJSON $TestString)")
Write-host "`n`n--------------------Test 2: Aggregate Function--------------------"
Format-CustomString $TestString

Write-host "`n`n--------------------Test 3--------------------"
$TestString='(id,created,employee(id,name(firstname,middlename,lastname),employeeType(id),title),location,GPSCoords)'
Write-output "Original:`n  $TestString"
write-output "JSON String:`n  $(Convert-CustomToJSON $TestSTring)"
write-output "PS Object:`n  $(Convert-JSONToPSObject "$(Convert-CustomToJSON $TestString)")"
write-output "Property Name Output:"
Write-PropertyNames $(Convert-JSONToPSObject "$(Convert-CustomToJSON $TestString)")
Write-host "`n`n--------------------Test 3: Aggregate Function--------------------"
Format-CustomString $TestString

Write-host "`n`n--------------------Test 4--------------------"
$TestString="(id,created,employee(id,name(firstname,middlename,lastname),employeeType(id)),location,GPSCoords)"
Write-output "Original:`n  $TestString"
write-output "JSON String:`n  $(Convert-CustomToJSON $TestSTring)"
write-output "PS Object:`n  $(Convert-JSONToPSObject "$(Convert-CustomToJSON $TestString)")"
write-output "Property Name Output:"
Write-PropertyNames $(Convert-JSONToPSObject "$(Convert-CustomToJSON $TestString)")
Write-host "`n`n--------------------Test 4: Aggregate Function--------------------"
Format-CustomString $TestString

Write-host "`n`n--------------------Test 4.1 Aggregate Function Descending Order--------------------"
$TestString="(id,created,employee(id,name(firstname,middlename,lastname),employeeType(id)),location,GPSCoords)"
write-output "Property Name Output:"
Write-PropertyNames -Descending $(Convert-JSONToPSObject "$(Convert-CustomToJSON $TestString)")