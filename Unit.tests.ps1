
$LibraryRoot="$(Split-Path $myinvocation.Mycommand.path -Parent)"
Write-Host "Loading $LibraryRoot\DataTranformationLibrary.ps1"
. "$LibraryRoot\DataTranformationLibrary.ps1"

Describe 'Testing Convert-CustomToJSON' {
  It 'Should convert custom format to JSON' {
    Convert-CustomToJSON '(id,created,employee(id,firstname,employeeType(id),lastname),location)' | Should Be '{"id": "1","created": "1","employee": {"id": "1","firstname": "1","employeeType": {"id": "1"},"lastname": "1"},"location": "1"}'
  }
  It 'Should handle whitespace during format to JSON' {
    Convert-CustomToJSON '(id,    created,    employee(id, firstname,employeeType(id), lastname) ,location) ' | Should Be '{"id": "1","created": "1","employee": {"id": "1","firstname": "1","employeeType": {"id": "1"},"lastname": "1"},"location": "1"}'
  }
  It 'Should handle multiple closing parenthesis during format to JSON' {
    (Convert-CustomToJSON '(top(record(id,created,employee(id,name(firstname,middlename,lastname),employeeType(id)),location,GPSCoords)))' | convertfrom-json).gettype().Name | Should Be 'PSCustomObject'
  }
}

Describe 'Testing Convert-JSONToPSObject' {
  It 'Should convert valid JSON to PSObject' {
    (Convert-JSONToPSObject '{"top": {"record": {"id": "1","created": "1","employee": {"id": "1","name": {"firstname": "1","middlename": "1","lastname": "1"},"employeeType": {"id": "1"}},"location": "1","GPSCoords": "1"}}}')
  }
  It 'Should generate an error when data is malformed' {
    {Convert-CustomToJSON '(id,created,employee(id,firstname,employeeType(id),lastname)location)'| convertfrom-json } | Should -Throw "Conversion from JSON failed with error"
  }
}

Describe 'Testing Write-PropertyNames' {
  It 'Should produce expected output' {

$expectedoutput = @'
created
employee
-employeeType
--id
-firstname
-id
-lastname
id
location
'@

Write-PropertyNames (Convert-JSONToPSObject (Convert-CustomToJSON '(id,created,employee(id,firstname,employeeType(id), lastname),location)')) | out-string | Should Match $ExpectedOutput
  }

  It '-Descending Should produce reverse sorted output' {

$expectedoutput = @'
location
id
employee
-lastname
-id
-firstname
-employeeType
--id
created
'@

Write-PropertyNames -Descending -InputObject (Convert-JSONToPSObject (Convert-CustomToJSON '(id,created,employee(id,firstname,employeeType(id), lastname),location)')) | out-string | Should Match $ExpectedOutput
  }
}

Describe 'Testing Format-CustomString' {
  It 'Should produce expected output' {

$expectedoutput = @'
created
employee
-employeeType
--id
-firstname
-id
-lastname
id
location
'@

Format-CustomString '(id,created,employee(id,firstname,employeeType(id), lastname),location)' | out-string | Should Match $ExpectedOutput
  }
}
