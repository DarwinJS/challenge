### Runs on Linux / MacOS / Windows

This code and CI runs on PowerShell Core (on Linux, MacOS or Windows)
To install PowerShell Core, simply run the bash installer that I maintain as part of the PowerShell Open source project:

#### Prepare Non-Windows with PowerShell Core
````
#Non-Windows
curl -s https://raw.githubusercontent.com/PowerShell/PowerShell/master/tools/install-powershell.sh | bash
pwsh -c 'install-module pester -force -skippublishercheck'
````
#### Prepare Windows w/ PowerShell Core
````
#Windows with PowerShell Core (installs chocolatey automatically): 
If (!(Test-Path env:chocolateyinstall)) {iwr https://chocolatey.org/install.ps1 -UseBasicParsing | iex} ; cinst -y powershell-core
pwsh -c 'install-module pester -force -skippublishercheck'
````

#### Windows PowerShell (not completely working)
````
#Windows PowerShell on Windows 10 and 2016 must be forced to install pester 4.x and then use it rather than 3.x 
(because Microsoft pre-shipped pester 3.x as part of the OS install - so now it can't be removed - grrrrr )
#IMPORTANT: But even then 1 pester test gives a different result than is experienced under PowerShell Core so it incorrectly fails
#  This could likely be resolved with enough attention to updating the pester tests - however, I built on core and was only counter testing
#  on PowerShell Core
If ((get-module pester -listavailable | sort-object -property version | select-object -last 1 | select-object -expand version) -lt [version]"4.7")
{
  Write-Host "Installing Pester 4"
  install-module pester -force -skippublishercheck
}
Write-Host "Forcing the use of the latest pester"
import-module -FullyQualifiedName (get-module pester -listavailable | sort-object -property version | select-object -last 1).path

````

### Implementation
The code should handle any string whose format is consistent with the sample.

The implementation creates a JSON object by setting every attribute to a value of "1".  Since this would appear to be a schema for a record format, the use of JSON means that this functionality would work even if real data values were being populated into the JSON.

Moving the data to JSON was done to:
* Get to a format supported by PowerShell's built-in language constructs - which minimizes the net new, less tested code written to support this solution (JSON is easy to turn into PowerShell Objects).
* Since PowerShell has a rich metadata subsystem, the manipulation of "label" or "field" data can be done compactly and, again, without crafting custom code.
* To allow the built-in JSON support to do the heavy lifting for finding malformed source string data (with enough test data would also reveal any flaws in the code for going from custom format to JSON string)
* Since the data appears to be a record schema - the next steps would likely be to populate it with data, so moving the schema "JSON with fake data" is reusable once data is introduced into the solution.

The main code file ("DataTransformationLibrary.ps1") includes the simple functions developed during prototyping in order to show the simplicity of the core code.  These then overwritten by full best practice PowerShell implementations which enable pipelining, parameter validation and built-in language help.

Since the given string appears to be a data format, the implementation does not bother tracking the original order of the fields - but does provide the option to sort the labels in ascending or descending order.  If the field order that occurs in the data is actually critical to the usage of this code, the code would need to be enhanced to track the order metadata.

Implementing as a PowerShell module was specifically avoided as it adds a lot of review overhead for anyone who is not familiar with PowerShell modules and frequently the benefits of modules do not offset the distribution, installation and "single version backward compatibility" problems of any shared code mechanism when modules are formally installed into the system-wide module folders.  Modules can also be used dynamically without install which avoids many of these problems - but that is not a commonly advocated pattern.

Excuse my committing directly to master in the repository for this project - I normally use proper git work flows - but it would have slowed this side project significantly to generate a commit history that reflect proper git merge procedures.

### Testing

#### Gitlab-CI Unit Tests
A DevOps solution that I would be "proud of" includes unit tests.

This CI re-uses an Amazon Linux 2 container I build monthly that contains PowerShell Core preinstalled.

Gitlab CI does not yet parse NUnit test results - so they are only visible as a file artifact rather than in the Pipelines UI.

(CI results for this code)[https://gitlab.com/DarwinJS/challenge/pipelines]

#### Human friendly tests:

````
.\InteractiveTest.ps1
````

#### To run test framework tests manually, install and Pester according to your OS scenario above, then:

````

invoke-pester .\Unit.tests.ps1
````

### Getting Usage Help
Dot Source this to load the functions in a PowerShell session:
````
. .\DataTranformationLibrary.ps1

#Then get help:
Get-Help Convert-CustomToJSON -Detailed
Get-Help Convert-JSONToPSObject -Detailed
Get-Help Write-PropertyNames -Detailed
Get-Help Format-CustomString -Detailed
````