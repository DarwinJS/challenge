<#
This Runs on PowerShell Core (on linux or macOS)

To run tests:
install-module pester -force -skippublishercheck

Dot Source this to load the functions in a powershell session:
. .\DataTranformationLibrary.ps1

Then get help:
Get-Help Convert-CustomToJSON -Detailed
Get-Help Convert-JSONToPSObject -Detailed
Get-Help Write-PropertyNames -Detailed
Get-Help Format-CustomString -Detailed
#>


#Region Simple Implementation
# These are overwritten by later function declarations, but are included to show non-decorated simplicity 
# similar to languages that do not have integrated metadata or pipelining
Function Convert-CustomToJSON ($StringToProcess) {
   Return $($StringToProcess  -replace "\s",'' -replace '(\w+),','"$1": "1",' -replace '(\w+)\)','"$1": "1"}' -replace '(\w+)\(','"$1": {' -replace '\)','}' -replace '\(','{')
}

Function Convert-JSONToPSObject ($StringToProcess) {
  Try { Return $(ConvertFrom-JSON -InputObject $StringToProcess) }
  Catch { Throw "The data format of this string is unrecognized or malformed: '$stringtoprocess'"}
}

Function Write-PropertyNames ($InputObject) {
  if ((Get-PSCallStack)[1].Command -eq $MyInvocation.MyCommand) {$prepend = $prepend + '-'; $Recursing=$True} 
  $InputObject | get-member -membertype noteproperty | %{ write-output "$prepend$($_.Name)" ; If ($_.Definition -ilike '*PSCustomObject*') { Write-PropertyNames $InputObject.($_.Name)} }
  if ($Recursing) {$prepend = $prepend.Substring(1,$prepend.length-1)}
}

Function Format-CustomString ($StringToProcess) {
  Return Write-PropertyNames (Convert-JSONToPSObject (Convert-CustomToJSON $StringToProcess))
}
#EndRegion Simple Implementation

#Region PowerShell Best Practice Implementation
# including pipeline processing and full self describing help - use "get-help (Function-Name) -detailed"
Function Convert-CustomToJSON () {
<#
.SYNOPSIS
  Converts the custom format to a JSON string
.DESCRIPTION
  Input data format example: '(id,created,employee(id,firstname,employeeType(id), lastname),location)'
.PARAMETER StringToProcess
  Can be piplined into CMDLet.
.EXAMPLE
  Convert-CustomToJSON '(id,created,employee(id,firstname,employeeType(id), lastname),location)'

  {"id": "1","created": "1","employee": {"id": "1","firstname": "1","employeeType": {"id": "1"},"lastname": "1"},"location": "1"}
.EXAMPLE
  '(id,created,employee(id,firstname,employeeType(id), lastname),location)', '(id,created,employee(id,name(firstname,middlename,lastname),employeeType(id),title),location,GPSCoords)' | Convert-CustomToJSON

  {"id": "1","created": "1","employee": {"id": "1","firstname": "1","employeeType": {"id": "1"},"lastname": "1"},"location": "1"}
  {"id": "1","created": "1","employee": {"id": "1","name": {"firstname": "1","middlename": "1","lastname": "1"},"employeeType": {"id": "1"},"title": "1"},"location": "1","GPSCoords": "1"}

  #Example shows piplining multiple values
#> 
  [CmdletBinding()]
  param (
    [Parameter(Mandatory=$True,ValueFromPipeline=$True)][string]$StringToProcess
  )
  Process {
   Return $($StringToProcess  -replace "\s",'' -replace '(\w+),','"$1": "1",' -replace '(\w+)\)','"$1": "1"}' -replace '(\w+)\(','"$1": {' -replace '\)','}' -replace '\(','{')
  }
}

Function Convert-JSONToPSObject () {
<#
.SYNOPSIS
  Converts a JSON string to a PowerShell Object with pipelining and error suport.
.DESCRIPTION
  Input data format example (JSON): {"id": "1","created": "1","employee": {"id": "1","firstname": "1","employeeType": {"id": "1"},"lastname": "1"},"location": "1"}
.PARAMETER JSONStringToProcess
  Can be piplined into CMDLet.
.EXAMPLE
  Convert-JSONToPSObject '{"id": "1","created": "1","employee": {"id": "1","firstname": "1","employeeType": {"id": "1"},"lastname": "1"},"location": "1"}'

  id created employee                                        location
  -- ------- --------                                        --------
  1  1       @{id=1; firstname=1; employeeType=; lastname=1} 1
.EXAMPLE
  '{"id": "1","created": "1","employee": {"id": "1","firstname": "1","employeeType": {"id": "1"},"lastname": "1"},"location": "1"}', '{"id": "1","created": "1","employee": {"id": "1","name": {"firstname": "1","middlename": "1","lastname": "1"},"employeeType": {"id": "1"},"title": "1"},"location": "1","GPSCoords": "1"}' | Convert-JSONToPSObject

  id created employee                                        location
  -- ------- --------                                        --------
  1  1       @{id=1; firstname=1; employeeType=; lastname=1} 1
  1  1       @{id=1; name=; employeeType=; title=1}          1

  #Example shows piplining multiple values
.EXAMPLE
  '(id,created,employee(id,firstname,employeeType(id), lastname),location)' | Convert-CustomToJSON | Convert-JSONToPSObject
  
  id created employee                                        location
  -- ------- --------                                        --------
  1  1       @{id=1; firstname=1; employeeType=; lastname=1} 1

  #Example shows pipelining through multiple functions
#> 
  [CmdletBinding()]
  param (
    [Parameter(Mandatory=$True,ValueFromPipeline=$True)][string]$JSONStringToProcess
  )
  Process {
    Try { Return $(ConvertFrom-JSON -InputObject $JSONStringToProcess) }
    Catch { Throw "The data format of this string is unrecognized or malformed: '$JSONStringToProcess'"}
  }
}

Function Write-PropertyNames () {
<#
.SYNOPSIS
  Writes formatted list of property names
.DESCRIPTION
  Input data format example (JSON): [PSCustomObject]
.PARAMETER InputObject
  Can be piplined into CMDLet.
.EXAMPLE
  Write-PropertyNames Convert-JSONToPSObject '{"id": "1","created": "1","employee": {"id": "1","firstname": "1","employeeType": {"id": "1"},"lastname": "1"},"location": "1"}'

  Output is a bit long to include in the example, please run it to see output

.EXAMPLE
  Write-PropertyNames -SortOrder 'Descending' -InputObject $(Convert-JSONToPSObject '{"id": "1","created": "1","employee": {"id": "1","firstname": "1","employeeType": {"id": "1"},"lastname": "1"},"location": "1"}')

  Output is a bit long to include in the example, please run it to see output

  #Example shows properties in descending order
.EXAMPLE
  '{"id": "1","created": "1","employee": {"id": "1","firstname": "1","employeeType": {"id": "1"},"lastname": "1"},"location": "1"}', '{"id": "1","created": "1","employee": {"id": "1","name": {"firstname": "1","middlename": "1","lastname": "1"},"employeeType": {"id": "1"},"title": "1"},"location": "1","GPSCoords": "1"}' | Convert-JSONToPSObject | Write-PropertyNames

  Output is a bit long to include in the example, please run it to see output

  #Example shows piplining multiple values
.EXAMPLE
  '(id,created,employee(id,firstname,employeeType(id), lastname),location)' | Convert-CustomToJSON | Convert-JSONToPSObject | Write-PropertyNames

  Output is a bit long to include in the example, please run it to see output

  #Example shows pipelining through multiple functions
#> 
  [CmdletBinding()]
  param (
    [Parameter(Mandatory=$True,ValueFromPipeline=$True)]$InputObject,
    [Parameter(Mandatory=$False,ValueFromPipeline=$False)][switch]$Descending
  )
  Process {
    if ((Get-PSCallStack)[1].Command -eq $MyInvocation.MyCommand) {$prepend = $prepend + '-'; $Recursing=$True} 
    If ($Descending) {
      Return $($InputObject | get-member -membertype noteproperty | sort-object -Property Name -Descending | %{ write-output "$prepend$($_.Name)" ; If ($_.Definition -ilike '*PSCustomObject*') { Write-PropertyNames $InputObject.($_.Name) -Descending} }) 
    }
    else {
      Return $($InputObject | get-member -membertype noteproperty | sort-object -Property Name | %{ write-output "$prepend$($_.Name)" ; If ($_.Definition -ilike '*PSCustomObject*') { Write-PropertyNames $InputObject.($_.Name)} }) 
    }
    #Return $($InputObject | get-member -membertype noteproperty | sort-object -Property Name $SortOptions | %{ write-output "$prepend$($_.Name)" ; If ($_.Definition -ilike '*PSCustomObject*') { Write-PropertyNames $InputObject.($_.Name)} })
    if ($Recursing) {$prepend = $prepend.Substring(1,$prepend.length-1)}
  }
}

Function Format-CustomString () {
<#
.SYNOPSIS
  Writes formatted list of property names from the custom string by binding together 3 functions
.DESCRIPTION
  Input data format example (JSON): "(id,created,employee(id,firstname,employeeType(id), lastname),location)"
.PARAMETER StringToProcess
  Can be piplined into CMDLet.
.EXAMPLE
  Format-CustomString '(id,created,employee(id,firstname,employeeType(id), lastname),location)'

  Output is a bit long to include in the example, please run it to see output

.EXAMPLE
  '(id,created,employee(id,firstname,employeeType(id), lastname),location)', '(id,created,employee(id,name(firstname,middlename,lastname),employeeType(id),title),location,GPSCoords)' | Convert-CustomToJSON

  Output is a bit long to include in the example, please run it to see output

  #Example shows piplining multiple values
#> 
  [CmdletBinding()]
  param (
    [Parameter(Mandatory=$True,ValueFromPipeline=$True)]$StringToProcess
  )
  Process {
     Return Write-PropertyNames (Convert-JSONToPSObject (Convert-CustomToJSON $StringToProcess))
  }
}
#EndRegion PowerShell Best Practice


